const express = require('express');
// const randomId = require('random-id');
const app = express(),
      bodyParser = require("body-parser"),
      fs = require('fs'),
      port = 8080;

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const customCss = fs.readFileSync((process.cwd()+"/swagger.css"), 'utf8');
const defaultFilePath = __dirname + "/files/";

app.use(bodyParser.json());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, {customCss}));

app.post('/api/files', (req, res) => {
  const {filename, content} = req.body;
  if (fs.existsSync(defaultFilePath + filename)) {
    console.log(`Error creating file with name ${filename} because it already exists`);
    return res.status(400).json({message: "File with that name already exists"});
  }
  fs.writeFile(defaultFilePath + filename, content, function (err) {
    if (err) {
      console.log(`error: ${err}`);
      return res.status(500).json({message: err});
    }
  });
  console.log(`File with name ${filename} was successfully created`);
  return res.sendStatus(200);
});

app.get('/api/files', (req, res) => {
  filesArr = new Array();
  fs.readdir('./files/', (err, files) => {
    return res.status(200).send(files);
  });
})

app.get('/api/files/:filename', (req, res) => {
  var filename = req.params.filename;
  if (!fs.existsSync(defaultFilePath + filename)) {
    console.log(`Error creating file with name ${filename} because it already exists`);
    return res.status(400).json({message: "File with that name does not exist"});
  }

  fs.readFile(defaultFilePath + req.params.filename, 'utf8' , (err, data) => {
    if (err) {
      return res.status(500).json({message: err});
    }
    return res.status(200).json({ content: data });
  })
})

app.listen(port, () => {
    console.log(`Server listening on the port::::::${port}`);
});